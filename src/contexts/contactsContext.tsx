import { IPerson } from '@lib/models/person';
import { createContext, useState } from 'react';

export const ContactContext = createContext([]);

export const ContactProvider: React.FC<{ children: React.ReactNode }> = ({ children }) => {
  const [contacts, setContacts] = useState<IPerson[]>([]);

  const setContactsAndStore =(contacts: IPerson[]) => {
    localStorage.setItem('contacts', JSON.stringify(contacts));
    setContacts(contacts);
  }
  
  return <ContactContext.Provider value={[contacts, setContactsAndStore]}>{children}</ContactContext.Provider>;
};
