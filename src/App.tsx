import { RouterProvider, createBrowserRouter, Navigate, Outlet } from 'react-router-dom';

import { UIProvider } from '@contexts/uiContext';
import { routeDefinitions } from '@lib/routes';

import { MainLayout } from '@components/layouts';
import { ContactListPage, ContactEditPage } from '@components/pages';
import { ContactProvider } from '@contexts/contactsContext';

const appRouter = createBrowserRouter([
  {
    path: '/',
    element: <MainLayout />,
    children: [
      {
        path: '/',
        element: <Navigate to="/contacts" />
      },
      {
        path: '/contacts',
        element: <Outlet />,
        children: [
          {
            path: '/contacts',
            element: <ContactListPage />,
            id: routeDefinitions.contactsList.id
          },
          {
            path: '/contacts/edit/:id',
            element: <ContactEditPage />,
            id: routeDefinitions.contactEdit.id
          }
        ]
      }
    ]
  }
]);

function App() {
  return (
    <UIProvider>
      <ContactProvider>
      <RouterProvider router={appRouter} />
      </ContactProvider>
    </UIProvider>
  );
}

export default App;
