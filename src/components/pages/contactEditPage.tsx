import { ContactContext } from '@contexts/contactsContext';
import { IPerson } from '@lib/models/person';
import { useContext, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

import { Button, styled, TextField } from '@mui/material';

export const EditPageContainer = styled('div')(({ theme }) => ({
  background: '#FFF',
  padding: theme.spacing(3)
}));

export const ContactEditPage = () => {
  const [contactsContext, setContactsContext] = useContext(ContactContext);
  const [contactState, setContactState] = useState<IPerson>(null);
  const { id } = useParams();
  
  
  useEffect(() => {
    const contactRef = contactsContext.find((c: IPerson) => c.id === id);
    setContactState(contactRef);
  }, []);


  const submitContact = (e: any) => {
      e.preventDefault();
      console.log("Contact: ",contactState);
  }

  return (
    <EditPageContainer>
      {contactState && (
        <form onSubmit={submitContact}>
          <TextField
            type="text"
            label="First Name"
            value={contactState.firstName}
            onChange={e =>
              setContactState({ ...contactState, firstName: e.target.value })
            }></TextField>
          <TextField
            type="email"
            label="Email"
            value={contactState.email}
            onChange={e =>
              setContactState({ ...contactState, email: e.target.value })
            }></TextField>
          <Button type="submit" variant="text">Text</Button>
        </form>
      )}
    </EditPageContainer>
  );
};
